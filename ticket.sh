function ticket() {
    if [[ -n "$1" ]]; then
        arg1="$1"
        git fetch -q
        branch=$(git branch -a | grep -woie "\w*\/${1}[a-zA-Z\-]*" | head -1)

        if [[ -n $branch ]]; then
            git checkout -q $branch
        else
            echo "Ticket '$arg1' not found."
        fi
    fi
}
