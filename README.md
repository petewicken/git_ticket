Tickets
=======

Easily checkout branches in git referencing Jira ticket IDs.
Expects branches in the format of:

``feature/TK-123-my-ticket``

Or the remote equivalent:
``remotes/origin/feature/TK-123-my-ticket``

Where TK-123 is the ticket ID.
